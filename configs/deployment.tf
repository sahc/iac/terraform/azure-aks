
resource "azurerm_resource_group" "demo" {
  name     = "${var.resource_group_name}"
  location = "${var.location}"
  tags     = "${var.tags}"
}


resource "azurerm_container_registry" "demo" {
  name                = "${var.acr_name}"
  resource_group_name = "${azurerm_resource_group.demo.name}"
  location            = "${azurerm_resource_group.demo.location}"
  admin_enabled       = false
  sku                 = "${var.acr_sku}"
  tags                = "${var.tags}"
  depends_on          = ["azurerm_kubernetes_cluster.demo"]
}

resource "azurerm_kubernetes_cluster" "demo" {
  name                = "${var.aks_name}"
  location            = "${azurerm_resource_group.demo.location}"
  resource_group_name = "${azurerm_resource_group.demo.name}"
  kubernetes_version  = "${var.k8s_version}"

  linux_profile {
    admin_username = "${var.admin_user}"

    ssh_key {
      key_data = "${file("${var.ssh_public_key}")}"
    }
  }

  dns_prefix = "${var.aks_dns_prefix}"

  agent_pool_profile {
    name    = "default"
    count   = "${var.aks_agent_vm_count}"
    vm_size = "${var.aks_agent_vm_size}"
    os_type = "Linux"
  }

  service_principal {
    client_id     = "${var.client_id}"
    client_secret = "${var.client_secret}"
  }

  tags = "${var.tags}"
}