#tenant_id 			= "11"
#subscription_id 	= ""
#client_id 			= ""
#client_secret 		= ""


location 			= "East US"
resource_group_name	= "POC-AKS-Terraform"

ssh_public_key="../security/axityPublicImportedPuttyGenKey.txt"
admin_user = "arquitectura"

tags = {
  Responsible = "Sergio Hume"
  Environment = "Axity POC AKS Terraform"
}
aks_name="axityaks"
acr_name="axitypoc"
acr_sku="Basic"
aks_agent_vm_count = "2"
aks_agent_vm_size = "Standard_D2_v2"
aks_dns_prefix = "axity-aks"
k8s_version = "1.11.3"