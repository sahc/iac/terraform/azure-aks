/* Configure Azure Provider and declare all the Variables that will be used in Terraform configurations */
provider "azurerm" {
  subscription_id 	= "${var.subscription_id}"
  client_id 		= "${var.client_id}"
  client_secret 	= "${var.client_secret}"
  tenant_id 		= "${var.tenant_id}"
  skip_provider_registration = true
}

variable "subscription_id" {
  description = "Enter Subscription ID for provisioning resources in Azure"
}

variable "client_id" {
  description = "Enter App ID for Application created in Azure AD and RBAC"
}

variable "client_secret" {
  description = "Enter Password for Application in Azure AD"
}

variable "tenant_id" {
  description = "Enter Tenant ID / Directory ID of your Azure AD. Run Get-AzureSubscription to know your Tenant ID"
}

variable "location" {
  description = "The default Azure region for the resource provisioning"
}

variable "resource_group_name" {
  description = "Resource group name that will contain various resources"
}


/*Inicio nuevo */

variable "ssh_public_key" {
  type = "string"
}

variable "admin_user"{
  type = "string"
  default = "arquitectura"
}

variable "aks_dns_prefix" {
  type = "string"
}

variable "tags" {
  type = "map"

  default = {
    Environment = "Axity POC AKS Terraform"
    Responsible = "Sergio Hume"
  }
}

variable "acr_name" {
  description = "Name of Azure Container Registry, [name].azurecr.io"
  type = "string"
}

variable "acr_sku" {
  type    = "string"
  default = "Basic"
}

variable "aks_name" {
  type = "string"
}

variable "aks_agent_vm_size" {
  type    = "string"
  default = "Standard_D2_v2"
}

variable "aks_agent_vm_count" {
  type    = "string"
  default = "2"
}

variable "k8s_version" {
  type    = "string"
  default = "1.9.6"
}